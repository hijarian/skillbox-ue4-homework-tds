// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillBox_UE2_TDSGameMode.generated.h"

UCLASS(minimalapi)
class ASkillBox_UE2_TDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkillBox_UE2_TDSGameMode();
};



